#!/bin/bash
# Test camera and settings

DEVICE="/dev/video2"

# List camera devices
v4l2-ctl --list-devices

# List options
v4l2-ctl --device=$DEVICE --all

# Fix auto focus and exposure
v4l2-ctl --device=$DEVICE --set-ctrl=white_balance_temperature_auto=0
#v4l2-ctl --device=$DEVICE --set-ctrl=focus_auto=0
v4l2-ctl --device=$DEVICE --set-ctrl=gain=0
v4l2-ctl --device=$DEVICE --set-ctrl=exposure_auto=1
v4l2-ctl --device=$DEVICE --set-ctrl=exposure_absolute=20

# Launch Webcam (test) - Ctrl-C to continue
#mplayer tv:// -tv driver=v4l2:width=1280:height=720:device=$DEVICE:fps=15
mplayer tv:// -tv driver=v4l2:width=1920:height=1080:device=$DEVICE:fps=30


