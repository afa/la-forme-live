#include "SceneManager.h"

using namespace ofxCv;
using namespace cv;

SceneManager::SceneManager(){}

void SceneManager::setup(){

    // Camera setup
    camera.initGrabber(WIDTH, HEIGHT);
    camera.setDesiredFrameRate(30);

    // Load media resources
    videoIdle.load(VIDEO_IDLE);
    videoWalk.load(VIDEO_WALK);
    imageMap.load(IMAGE_MAP);
    videoWalk.setLoopState(OF_LOOP_PALINDROME);

    // OSC settings
    oscSender.setup(HOST, PORT);
    oscReceiver.setup(PORT_RECEIVER);

    // Init lines
    for (int i = 1; i < NB_POINTS ; i++) {
        Line lineTemp;
        lineTemp.a = points[i-1];
        lineTemp.b = points[i];
        lines.push_back(lineTemp);
    }

    // Last Line
    Line lineTemp;
    lineTemp.a = points[NB_POINTS-1];
    lineTemp.b = points[0];
    lines.push_back(lineTemp);

    // Init shapes
    for (int i = 0; i < NB_SHAPES ; i++) {
        Shape shapeTemp;
        shapeTemp.path.clear();
        ofPoint pTemp = points[shapesPoints[i][0]];
        shapeTemp.path.moveTo(pTemp.x, pTemp.y);
        for (int j = 1; j < 4 ; j++) {
            if ( shapesPoints[i][j] != -1 ) { // avoid last points if -1
                ofPoint pTemp2 = points[shapesPoints[i][j]];
                shapeTemp.path.lineTo(pTemp2.x, pTemp2.y);
            }
        }
        shapeTemp.path.close();
        shapeTemp.closed = true;
        shapeTemp.fill = 1;
        shapes.push_back(shapeTemp);
    }

    // Init OpenCv
    blobColor.allocate(WIDTH, HEIGHT);
    blobGray.allocate(WIDTH, HEIGHT);
    blobBg.allocate(WIDTH, HEIGHT);
    blobDifference.allocate(WIDTH, HEIGHT);

    // Start direct (test)
    //change(3);
}

void SceneManager::update() {

    switch (m_sceneIndex) {

    case 1:
        videoIdle.update();
        break;
    case 2:
        videoWalk.update();
        break;
    case 3:
        camera.update();
        if (camera.isFrameNew()) {

            if (m_mapDraw > 0) {
            // Map image
            ofxCv::copy(imageMap, imageMapNew);
            ofxCv::GaussianBlur(imageMapNew, m_mapBlur);
            imageMapNew.update(); //refresh the image from the pixels.
            //setBrightness(imageMapNew, m_brightness, 4);
            //setContrast(imageMapNew, m_contrast);
            }

            // Shape style
            if ( shape.fill > 0 ){ shape.path.setFilled(true); }
            else { shape.path.setFilled(false); }
            shape.path.setColor(ofColor(shape.R, shape.G, shape.B, shape.A));
            shape.path.setStrokeWidth(shape.stroke);

            // Shapes styles
            if (m_drawShapes) {
                for (int i = 0; i < NB_SHAPES ; i++) {
                    if ( shapes[i].fill > 0 ){ shapes[i].path.setFilled(true); }
                    else { shapes[i].path.setFilled(false); }
                    shapes[i].path.setColor(ofColor(shapes[i].R, shapes[i].G, shapes[i].B, shapes[i].A));
                    shapes[i].path.setStrokeWidth(shapes[i].stroke);
                }
            }
        }
        break;
    case 4:
        camera.update();
        if (camera.isFrameNew()) {
            blobColor.setFromPixels(camera.getPixels());
            blobGray = blobColor;
            if (m_learn){
                blobBg = blobGray;
                m_learn = false;
            }
            blobDifference.absDiff(blobBg, blobGray);
            blobDifference.threshold(m_threshold);
            blobContour.findContours(blobDifference, 400, WIDTH * HEIGHT / 4, 7, false);

            ofxCv::copy(blobDifference, imageMapNew);
            ofxCv::GaussianBlur(imageMapNew, m_diffBlur);
            imageMapNew.update();

            int nBlobs = blobContour.blobs.size();

            // Send OSC blob
            ofxOscMessage m;
            m.setAddress("/blobs");
            m.addIntArg(nBlobs);
            oscSender.sendMessage(m, false);

            blobShape.path.clear();            
            blobShape.fill = (int) m_drawShapes;

            if ( blobShape.fill > 0 ){ blobShape.path.setFilled(true); }
            else { blobShape.path.setFilled(false); }
            blobShape.path.setColor(ofColor(blobShape.R, blobShape.G, blobShape.B, blobShape.A));
            if (m_drawLines) {blobShape.path.setStrokeWidth(blobShape.stroke);}
            else { blobShape.path.setStrokeWidth(0);}

            if (nBlobs > 1) {
                blobShape.path.moveTo(blobContour.blobs[0].centroid.x, blobContour.blobs[0].centroid.y);
                ofxOscMessage m;
                m.setAddress("/blob");
                m.addIntArg(0);
                m.addIntArg(blobContour.blobs[0].area);
                oscSender.sendMessage(m, false);

                for(unsigned int i = 1; i < nBlobs; i++) {
                    blobShape.path.lineTo(blobContour.blobs[i].centroid.x, blobContour.blobs[i].centroid.y);
                    // Send OSC blob
                    ofxOscMessage m;
                    m.setAddress("/blob");
                    m.addIntArg(i);
                    m.addIntArg(blobContour.blobs[i].area);
                    oscSender.sendMessage(m, false);

                }
                blobShape.path.close();
            }

        }
        break;
    }

    //OSC messages
    while(oscReceiver.hasWaitingMessages()){
        ofxOscMessage m;
        oscReceiver.getNextMessage(m);

        // Shape
        if(m.getAddress() == "/shape/stroke"){ shape.stroke = m.getArgAsInt32(0);}
        else if(m.getAddress() == "/shape/alpha"){ shape.A = m.getArgAsInt32(0) ;}
        else if(m.getAddress() == "/shape/fill"){ shape.fill = m.getArgAsInt32(0);}
        else if(m.getAddress() == "/shape/rgb"){
            shape.R = m.getArgAsInt32(0);
            shape.G = m.getArgAsInt32(1);
            shape.B = m.getArgAsInt32(2);
        }

        // Lines
        else if(m.getAddress() == "/lines/stroke"){ lines[m.getArgAsInt32(0)].stroke = m.getArgAsInt32(1) ;}
        else if(m.getAddress() == "/lines/alpha"){ lines[m.getArgAsInt32(0)].A = m.getArgAsInt32(1);}
        else if(m.getAddress() == "/lines/rgb"){
            lines[m.getArgAsInt32(0)].R = m.getArgAsInt32(1);
            lines[m.getArgAsInt32(0)].G = m.getArgAsInt32(2);
            lines[m.getArgAsInt32(0)].B = m.getArgAsInt32(3);
        }

        // Shapes
        else if(m.getAddress() == "/shapes/stroke"){ shapes[m.getArgAsInt32(0)].stroke = ofMap(m.getArgAsInt32(1), 0, 100, 0, 10) ;}
        else if(m.getAddress() == "/shapes/alpha"){ shapes[m.getArgAsInt32(0)].A = ofMap(m.getArgAsInt32(1), 0, 100, 0, 255) ;}
        else if(m.getAddress() == "/shapes/fill"){ shapes[m.getArgAsInt32(0)].fill = m.getArgAsInt32(1) ;}
        else if(m.getAddress() == "/shapes/rgb"){
            shapes[m.getArgAsInt32(0)].R = m.getArgAsInt32(1);
            shapes[m.getArgAsInt32(0)].G = m.getArgAsInt32(2);
            shapes[m.getArgAsInt32(0)].B = m.getArgAsInt32(3);
        }

        // Map
        else if(m.getAddress() == "/map/blur"){ m_mapBlur = m.getArgAsInt32(0) ; }
        else if(m.getAddress() == "/map/alpha"){ m_mapA = m.getArgAsInt32(0) ; }
        else if(m.getAddress() == "/map/rgb"){
            m_mapR = m.getArgAsInt32(0);
            m_mapG = m.getArgAsInt32(1);
            m_mapB = m.getArgAsInt32(2);
        }
        else if(m.getAddress() == "/map/draw"){ m_mapDraw = m.getArgAsInt32(0) ; }

        // Points
        else if(m.getAddress() == "/points/draw"){ m_pointsDraw = m.getArgAsInt32(0) ; }

        // Blob Diff
        else if(m.getAddress() == "/blob/diff/blur"){ m_diffBlur = m.getArgAsInt32(0) ; }
        else if(m.getAddress() == "/blob/diff/alpha"){ m_diffA = m.getArgAsInt32(0) ; }
        else if(m.getAddress() == "/blob/diff/rgb"){
            m_diffR = m.getArgAsInt32(0);
            m_diffG = m.getArgAsInt32(1);
            m_diffB = m.getArgAsInt32(2);
        }

        // Blob cam
        else if(m.getAddress() == "/blob/cam/alpha"){ m_diffCamA = m.getArgAsInt32(0) ; }

        // Blob Shape
        if(m.getAddress() == "/blob/shape/stroke"){ blobShape.stroke = m.getArgAsInt32(0);}
        else if(m.getAddress() == "/blob/shape/alpha"){ blobShape.A = m.getArgAsInt32(0) ;}
        else if(m.getAddress() == "/blob/shape/fill"){ blobShape.fill = m.getArgAsInt32(0);}
        else if(m.getAddress() == "/blob/shape/rgb"){
            blobShape.R = m.getArgAsInt32(0);
            blobShape.G = m.getArgAsInt32(1);
            blobShape.B = m.getArgAsInt32(2);
        }
    }
}

void SceneManager::draw(){
    switch (m_sceneIndex) {
    case 1:
        videoIdle.draw(0,0);
        break;
    case 2:
        videoWalk.draw(0,0);
        break;
    case 3:
        // Camera
        ofSetColor(255);
        camera.draw(0,0);

        // Map
        if (m_mapDraw > 0) {
        ofSetColor(m_mapR, m_mapG, m_mapB, m_mapA);
        imageMapNew.draw(0,0);
        }

        // Draw target (cross)
        ofSetLineWidth(1);
        ofSetColor(255);
        ofDrawLine(points[m_pointsIndex].x, points[m_pointsIndex].y + 10, points[m_pointsIndex].x, points[m_pointsIndex].y - 10 ); // vert.
        ofDrawLine(points[m_pointsIndex].x - 10, points[m_pointsIndex].y, points[m_pointsIndex].x + 10, points[m_pointsIndex].y ); // horiz.

        // Draw the points if the shape is not closed
        if ( m_pointsDraw > 0 ) {
            ofSetColor(255);
            for (int i=0; i < m_pointsIndex; i++){ ofDrawCircle(points[i], 5); };
        }

        // Draw shape
        shape.path.draw();

        // Draw shapes
        if (m_drawShapes) {
            for (auto sh : shapes) {
                sh.path.draw();
            }
        }

        // Draw lines
        if (m_drawLines) {
            for (auto line : lines) {
                ofSetColor(line.R, line.G, line.B, line.A);
                ofSetLineWidth(line.stroke);
                ofDrawLine(line.a, line.b);
            }
        }
        break;
    case 4:
        ofSetColor(m_diffR, m_diffG , m_diffB , m_diffA);
        imageMapNew.draw(0,0);
        ofSetColor(255, 255, 255, m_diffCamA);
        camera.draw(0,0);
        //blobDifference.draw(0, 0);

        blobShape.path.draw();
        break;
    }
}

void SceneManager::change(int _index){
    switch (_index) {
    case 1:
        videoIdle.play();
        break;
    case 2:
        videoWalk.play();
        break;
    case 3:
        m_pointsIndex = 0;
        shape.closed = 0;
        shape.path.clear();
        shape.path.moveTo(points[0].x, points[0].y);
        shape.path.setStrokeColor(ofColor::white);
        shape.path.setFillColor(ofColor::white);
        shape.stroke = 10;
        shape.fill = 0;
        shape.A = 100;
        shape.B = 255;
        shape.R = 255;
        shape.G = 255;

        m_drawShapes = false;
        m_drawLines = false;

        m_mapA = 255;
        m_mapR = 255;
        m_mapG = 255;
        m_mapB = 255;
        m_mapDraw = 1;
        m_pointsDraw = 1;

        break;
    case 4:
        m_learn = true;
        blobShape.closed = 0;
        blobShape.path.clear();
        blobShape.path.setStrokeColor(ofColor::white);
        blobShape.path.setFillColor(ofColor::white);
        blobShape.stroke = 10;
        blobShape.fill = 0;
        blobShape.A = 100;
        blobShape.B = 255;
        blobShape.R = 0;
        blobShape.G = 255;
        break;
    }

    // if the soft was played a first time we can unload/stop previous scenes
    if(m_bFirstStarted) {
        m_sceneLastIndex = m_sceneIndex;
        close(m_sceneLastIndex);
    } else {
        m_bFirstStarted = true;
    }

    m_sceneIndex = _index;

    // Send OSC scene index
    ofxOscMessage m;
    m.setAddress("/scene");
    m.addIntArg(m_sceneIndex);
    oscSender.sendMessage(m, false);
}


void SceneManager::close(int _index){
    switch (_index) {
    case 1:
        videoIdle.stop(); // or close but it is too slow
        break;
    case 2:
        videoWalk.stop();
        break;
    }
}

void SceneManager::recPoint(){
    if (!shape.closed) {
        if (m_pointsIndex < NB_POINTS) {
            shape.path.lineTo(points[m_pointsIndex].x, points[m_pointsIndex].y);

            // Send OSC points
            ofxOscMessage m;
            m.setAddress("/shape/points");
            m.addIntArg(m_pointsIndex);
            oscSender.sendMessage(m, false);

            m_pointsIndex++;
        } else {
            // Close shape
            shape.path.close();
            //tessellation = shape.getTessellation();
            shape.closed = true;

            // Send OSC close
            ofxOscMessage m;
            m.setAddress("/shape/close");
            oscSender.sendMessage(m, false);
        }
    }
}

void SceneManager::setContrast(ofImage &image, const int contrast) {
    int numChannels = 4;
    switch (image.getImageType()) {
    case OF_IMAGE_GRAYSCALE:
        numChannels = 1;
        break;
    case OF_IMAGE_COLOR:
        numChannels = 3;
        break;
    case OF_IMAGE_COLOR_ALPHA:
        numChannels = 4;
        break;
    default:
        break;
    }
    ofPixels &pix = image.getPixels();
    const size_t pixSize = static_cast<size_t>(image.getWidth() * image.getHeight() * numChannels);
    const float factor = (259.0f * (contrast + 255)) / (255.0f * (259 - contrast));

    if (numChannels == 1) {
        for (size_t i=0; i<pixSize; ++i) {
            const int g = static_cast<int>(factor * (pix[i] - 128) + 128);
            pix[i] = static_cast<unsigned char>(g < 0 ? 0 : g > 255 ? 255 : g);
        }
    }
    else {
        for (size_t i=0; i<pixSize; i+=numChannels) {
            const int r = static_cast<int>(factor * (pix[i] - 128) + 128);
            const int g = static_cast<int>(factor * (pix[i+1] - 128) + 128);
            const int b = static_cast<int>(factor * (pix[i+2] - 128) + 128);
            pix[i] = static_cast<unsigned char>(r < 0 ? 0 : r > 255 ? 255 : r);
            pix[i+1] = static_cast<unsigned char>(g < 0 ? 0 : g > 255 ? 255 : g);
            pix[i+2] = static_cast<unsigned char>(b < 0 ? 0 : b > 255 ? 255 : b);
        }
    }
    image.update();
}

void SceneManager::setBrightness(ofImage &image, const int brightness, const int nb_channels) {
    unsigned char * pix = image.getPixels().getData();
    for(int i = 0; i < 1280 * 720 * nb_channels; i++){ //for a 4 channel rgba image
        pix[i] += MIN(brightness, 255-pix[i]); //this makes sure it doesn't go over 255 as it will wrap to 0 otherwise.
    }
    image.update();
}

void SceneManager::sendTrigger(){
    ofxOscMessage m;
    m.setAddress("/trigger");
    oscSender.sendMessage(m, false);
}
