#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxOsc.h"
#include "ofxCv.h"

// Global settings
#define WIDTH 1920
#define HEIGHT 1080
#define NB_SCENE 5
#define NB_POINTS 7
#define NB_SHAPES 7

// Media files
#define VIDEO_IDLE "/home/jr/arts/la-forme/villes/04-neuss/live-media/idle.mp4"
#define VIDEO_WALK "/home/jr/arts/la-forme/villes/04-neuss/live-media/walk.mp4"
#define IMAGE_MAP "/home/jr/arts/la-forme/villes/04-neuss/live-media/neuss.png"

// OSC
#define HOST "localhost"
#define PORT 9000
#define PORT_RECEIVER 10000

class Line {
public:
    ofPoint a;
    ofPoint b;
    int stroke = 2;
    int R = 255;
    int G = 255;
    int B = 255;
    int A = 200;

};

class Shape {
public:
    ofPath path;
    int stroke = 2;
    int fill = 0;
    bool closed = 0;
    int R = 255;
    int G = 255;
    int B = 255;
    int A = 200;
};

class SceneManager {
public:
    SceneManager();
    void setup();
    void update();
    void draw();
    void change(int _i);
    void close(int _i);
    void recPoint();

    // getters
    int getSize(){ return m_sceneNb; } ;
    int getSceneIndex() { return m_sceneIndex; } ;
    bool getDrawShapes() { return m_drawShapes ;} ;
    bool getDrawLines() { return m_drawLines; };
    int getThreshold(){ return m_threshold; } ;

    //setters
    void setDrawShapes(bool _b) { m_drawShapes = _b;} ;
    void setDrawLines(bool _b) { m_drawLines = _b;} ;
    void setThreshold(int _t){ m_threshold = _t; } ;
    void setLearn(bool _b){ m_learn = _b; } ;

    void sendTrigger();

    // Image effects
    void setContrast(ofImage &image, const int contrast);
    void setBrightness(ofImage &image, const int bright, const int nb_channels);

    // Video
    ofVideoGrabber camera;
    ofVideoPlayer videoIdle;
    ofVideoPlayer videoWalk;

    // Images
    ofImage imageMap, imageMapNew;

    // Shape
    ofPoint points[NB_POINTS] = {
        ofPoint(875, 484),
        ofPoint(959, 651),
        ofPoint(1074, 763),
        ofPoint(1333, 647),
        ofPoint(1663, 388),
        ofPoint(1049, 562),
        ofPoint(1013, 286)
    };
    int shapesPoints [NB_SHAPES][4] = {
        { 0, 2, 5, 6},
        { 0, 1, 4, 5},
        { 1, 2, 3, 5},
        { 1, 4, 5, 6},
        { 1, 2, 4, -1},
        { 3, 4, 5, -1},
        { 0, 2, 3, -1}
    };
    Shape shape;
    vector <Line> lines;
    vector <Shape> shapes;
    ofVboMesh tessellation;

    // Blob tracking
    ofxCvColorImage	blobColor;
    ofxCvGrayscaleImage blobGray;
    ofxCvGrayscaleImage blobBg;
    ofxCvGrayscaleImage blobDifference;
    ofxCvContourFinder blobContour;
    Shape blobShape;

    ofxCvColorImage camContrast;
    ofImage img;
    //ofVboMesh blobShapeTessellation;

    // OSC
    ofxOscSender oscSender;
    ofxOscReceiver oscReceiver;

private:
    // Scene
    int m_sceneIndex = 0;
    int m_sceneLastIndex;
    int m_sceneNb = NB_SCENE;

    // Scene 3 - Map
    int m_mapBlur = 0;
    int m_mapDraw = 1;
    int m_mapA = 0;
    int m_mapR = 255;
    int m_mapG = 255;
    int m_mapB = 255;

    int m_pointsDraw = 1;

    // Scene 3 - shape
    bool m_drawLines = false;
    bool m_drawShapes = false;

    // Scene 4 - blob
    int m_threshold = 70;
    bool m_learn = true;

    int m_diffA = 255;
    int m_diffR = 255;
    int m_diffG = 255;
    int m_diffB = 255;
    int m_diffBlur = 0;

    int m_diffCamA = 100;

    int m_pointsIndex = 0;
    bool m_bFirstStarted = false;
};
