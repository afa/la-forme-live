#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    // Global setup
    ofBackground(0);
    ofEnableAlphaBlending();
    ofSetFrameRate(30);
    ofSetVerticalSync(true);
    ofHideCursor();
    // anti-aliasing

    bDelayStarted = false;
    delayEnd = 90000; // msec

    sm.setup();
}

//--------------------------------------------------------------
void ofApp::update(){
    sm.update();

    // Delay change scene : 0 -> 1
    if (bDelayStarted) {
        // update the timer this frame
        float timer = ofGetElapsedTimeMillis() - delayStart;
        if(timer >= delayEnd) {
            bDelayStarted = false;
            sm.change(2);
        }
    }

}

//--------------------------------------------------------------
void ofApp::draw(){
    sm.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    //cout << "KEY : " << key << endl;
    //Mapping: / 47, * 42, - 45, + 43, Backspace 8, Enter 13, Numbers 0-9 48-57, . 46

    // NUM KEYS
    if (key >= 48 && key <= (48 + sm.getSize() - 1) ) { // number from 0 to 9
        int num = key - 48; // convert to 0-9
        if (num == 2) {
           bDelayStarted = true;
           delayStart = ofGetElapsedTimeMillis(); // get the start time
        } else {
           bDelayStarted = false;
           sm.change(num);
        }
    } else if (key == 46 && sm.getSceneIndex() == 3) { // . : rec point
        sm.recPoint();
    } else if (key == 47) { // / : draw lines
        sm.setDrawLines(!sm.getDrawLines());
    } else if (key == 42 ) { // * : draw shapes
        sm.setDrawShapes(!sm.getDrawShapes());
    } else if (key == 43 && sm.getSceneIndex() == 4) { // +
        int t = sm.getThreshold();
        t++;
        if (t > 255) t = 255;
        sm.setThreshold(t);
    } else if (key == 45 && sm.getSceneIndex() == 4) { // -
        int t = sm.getThreshold();
        t--;
        if (t < 0) t = 0;
        sm.setThreshold(t);
    } else if (key == 13 && sm.getSceneIndex() == 4 ) { // Enter
        sm.setLearn(true);
    } else if (key == 8 && sm.getSceneIndex() == 4 ) { // Enter
        sm.sendTrigger();
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
