#include "ofMain.h"
#include "ofApp.h"

int main( ){
    ofSetupOpenGL(1920, 1080, OF_GAME_MODE); // fullscreen
    //ofSetupOpenGL(1280, 720, OF_WINDOW);
    //ofSetupOpenGL(1920, 1080, OF_WINDOW);
	ofRunApp(new ofApp());
}
