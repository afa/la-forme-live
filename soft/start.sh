#!/bin/bash

# Launch test script
# ./test_cam.sh

APP_LA_FORME="/home/jr/softs/of_v0.11.2_linux64gcc6_release/apps/myApps/appLaForme/bin/appLaForme"
PATCH_PD="/home/jr/arts/la-forme/la-forme-live/soft/pd/main.pd"
PREF_PD="/home/jr/arts/la-forme/la-forme-live/soft/pd/laforme.pdsettings"


#killall pd

# Launch Pd patch
#pd -open $PATCH_PD -prefsfile $PREF_PD &

#sleep 2

# Launch app (OpenFrameworks binary)
$APP_LA_FORME
