
/* 
 * 5 ANALOG SENSORS TO SERIAL (Pro Micro generic)
 * Proc: ATMEGA16u2
 * Board: Arduino Micro
 * Licence : GNU/GPL3
 * Date : 2022.05
 * * 
 * Pins: A3, A2, A1, A0, A10 
 */

#define ANA_NB 5
#define SAMPLING_RATE 40

unsigned long now = 0;

// Current and previous values
int anaPins[] = { A10, A0, A1, A2, A3 };
int anaCurrentValues[ANA_NB];
int anaPreviousValues[ANA_NB];

void setup()
{
  Serial.begin(9600);
  while (! Serial);
}

void loop()
{
  sendAna();
  delay(1);
}

void sendAna(){
    while(millis() > now + SAMPLING_RATE){
      for (int i = 0; i < ANA_NB; i++) {
        anaCurrentValues[i] = (int) analogRead(anaPins[i]);
        // hysteresis
        if (  (anaCurrentValues[i] >= (anaPreviousValues[i] + 2)) || (anaCurrentValues[i] <= (anaPreviousValues[i] - 2)) ) {
          Serial.print(i);
          Serial.print(" ");
          Serial.println(1023 - anaCurrentValues[i]);
        }
        anaPreviousValues[i] = anaCurrentValues[i];
      }

      now = millis();
    }
}
