# La Forme Live

Live session of La Forme.

- 28/05/2022  : La Forme de Neuss


## Camera settings
- sudo apt-get install v4l-utils mplayer v4l-utils v4l-conf
- v4l2-ctl --device=/dev/video0 --all
- v4l2-ctl --list-devices
- Use "v4linux control panel application"

## Launch Webcam fullscreen
- mplayer tv:// -tv driver=v4l2:width=1280:height=720:device=/dev/video0:fps=15 -fs
- mplayer tv:// -tv driver=v4l2:width=1920:height=1080:device=/dev/video0:fps=30 -fs
- ffplay /dev/video0
- mpv /dev/video0 --no-audio --fs 

## Setup 1
- framerate : v4l2-ctl -d /dev/video2 -p 30
- resolution : v4l2-ctl --device=/dev/video2 --set-fmt-video=width=1920,height=1080
- setup : v4l2-ctl --device=/dev/video2 --set-ctrl=exposure_auto=1,white_balance_temperature_auto=0,exposure_absolute=10,gain=20
- reset : v4l2-ctl --device=/dev/video2 --set-ctrl=exposure_absolute=156,exposure_auto=3,white_balance_temperature_auto=1,gain=0


## Setup 2 from askubuntu.com
v4l2-ctl \
--set-ctrl=brightness=150 \
--set-ctrl=contrast=51 \
--set-ctrl=saturation=32 \
--set-ctrl=white_balance_temperature_auto=0 \
--set-ctrl=gain=90 \
--set-ctrl=power_line_frequency=1 \
--set-ctrl=white_balance_temperature=1140 \
--set-ctrl=sharpness=24 \
--set-ctrl=backlight_compensation=1 \
--set-ctrl=exposure_auto=1 \
--set-ctrl=exposure_absolute=870 \
--set-ctrl=exposure_auto_priority=1

### /dev/camera2 settings
- brightness (int)	: min=-64 max=64 step=1 default=0 value=0
- contrast (int) 	: min=0 max=64 step=1 default=32 value=32
- saturation (int) 	: min=0 max=128 step=1 default=64 value=64
- hue (int)			: min=-40 max=40 step=1 default=0 value=0
- gamma (int) 		: min=72 max=500 step=1 default=100 value=100
- gain (int)    	: min=0 max=100 step=1 default=0 value=0
- sharpness (int)	: min=0 max=6 step=1 default=3 value=3
- white_balance_temperature_auto (bool)	: default=1 value=0
- white_balance_temperature (int) 	 	: min=2800 max=9300 step=1 default=4600 value=4600
- backlight_compensation (int)    		: min=0 max=2 step=1 default=1 value=1
- exposure_auto (menu)				: min=0 max=3 default=3 value=3 (1: Manual Mode, 3: Aperture Priority Mode)
- exposure_absolute (int)			: min=1 max=10000 step=1 default=156 value=156 flags=inactive
- exposure_auto_priority (bool)		: default=0 value=0
- power_line_frequency (menu)			: min=0 max=2 default=1 value=1 (0: Disabled, 1: 50 Hz, 2: 60 Hz)

## Map
- openstreetmap > export svg
- npm install -g svgo
- svgo map.svg (-50%)
- inkscape : all in black, stroke 0.5, no fill, dimensions of document 1920x1080, export as a png


### > posters
- image magick policy to convert: just remove this whole following section from /etc/ImageMagick-6/policy.xml:
'''
<!-- disable ghostscript format types -->
<policy domain="coder" rights="none" pattern="PS" />
<policy domain="coder" rights="none" pattern="PS2" />
<policy domain="coder" rights="none" pattern="PS3" />
<policy domain="coder" rights="none" pattern="EPS" />
<policy domain="coder" rights="none" pattern="PDF" />
<policy domain="coder" rights="none" pattern="XPS" />
'''
- convert map_NB.svg in.pdf
- pdfposter -mA3 -pA1 in.pdf out.pdf

### geojson > svg
cat laforme.geojson | npx geojson-to-svg-cli > example.svg

### iframe umap
<iframe width="100%" height="300px" frameborder="0" allowfullscreen src="//umap.openstreetmap.fr/fr/map/la-forme_705185?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>
